<?php
session_start();  
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Matka -ja päästölaskuri</title>
</head>
<body>
<?php  


class Journey {
    function __construct() {
        if (filter_input(INPUT_SERVER, 'REQUEST_METHOD') === "POST") {
            $_SESSION["vehicleType"] = filter_input(
                INPUT_POST, "vehicleType", FILTER_SANITIZE_STRING);
            $_SESSION["distance"] = filter_input(
                INPUT_POST, "distance", FILTER_SANITIZE_NUMBER_INT);       
        }
    }
}

$journey = new Journey();
 
if ($_SESSION["vehicleType"] === "Auto") {
    header("Location: car.html"); 
} 
else {
    header("Location: Calculator.php");
}
?>
</body>
</html>
