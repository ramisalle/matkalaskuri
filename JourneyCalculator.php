<?php

require_once("Calculator.php");
class JourneyCalculator extends Calculator {

    private $hours;
    private $minutes;
    private $seconds;
    private $time;

    /**
     * Assign distance to member variable of the class
     * 
     * @param integer $distance distance of the journey
     */

    public function setDistance($distance) {
      $this->distance = $distance;
    }

    public function setSpeed($speed) {
      $this->speed = $speed;
    } 

    
public function calculateTime() { 
$distanceInMeters = $this->distance * 1000;
$speedInMetersPerSecond = $this->speed * 0.27777777777777777777777777777;
$this->seconds = $distanceInMeters / $speedInMetersPerSecond;
$this->hours = floor($this->seconds / 3600);
$this->seconds -= $this->hours * 3600;
$this->minutes = floor($this->seconds / 60);
$this->seconds = floor($this->seconds - ($this->minutes * 60));
$this->formatTime();
return $this->time;
  } 

  /**
     * Format calculated time
     * 
     */
    
  private function formatTime(){
  if ($this->hours > 0) {
    $this->time = $this->hours . " h ";
  }
  if ($this->minutes > 0) {
    $this->time = $this->minutes . " min ";
  }
  if ($this->seconds > 0) {
  $this->time = $this->seconds . " sek ";

      }
    }
}
