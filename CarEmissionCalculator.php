<?php

require_once("EmissionCalculator.php");

define("BIODIESEL_ADVANTAGE_FACTOR", 0.36);
define("SPEED_DISADVANTAGE_LIMIT", 120);
define("SPEED_DISADVANTAGE_FACTOR", 0.25);

class CarEmissionCalculator extends EmissionCalculator {

    /* public function setPersons($persons) {
        $this->persons = persons;
    }

    public function setFuelType($fuelType) {
        $this->fuelType = $fuelType;
    }

    public function setSpeed($speed) {
        $this->speed = $speed;
    } */


public function calculateEmission() {
   if ($this->persons === "1") {
    $this->emission = $this->distance * CAR_MULTIPLY_FACTOR;
   }
   else {
       $this->emission = ($this->distance * CAR_MULTIPLY_FACTOR) / 2;
   }
    if ($this->fuelType === "Biodiesel") {
    $this->emission -= ($this->emission * BIODIESEL_ADVANTAGE_FACTOR);
    }
    if ($this->speed >= SPEED_DISADVANTAGE_LIMIT) {
        $this->emission += $this->emission * SPEED_DISADVANTAGE_FACTOR;
    }
    return ceil($this->emission);
  }
}
